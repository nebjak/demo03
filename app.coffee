mysql = require 'mysql2'
crypto = require 'crypto'

sql = mysql.createConnection
  host: 'localhost',
  user: 'tester',
  password: 'tester',
  database: 'demo03'


setInterval ->
  crypto.randomBytes 256, (err, buf) =>
    hash = crypto.createHash 'sha512'
    hash.update buf.toString('hex')
    data =
      hash: hash.digest("hex"),
      plain: buf.toString("hex")
      num: Math.random()/Math.random()
    sql.query "insert into datam set ?", data, (err, res) ->
      if err
        throw (err)
      else console.log("Successful insert: ", res.affectedRows)
, 200

